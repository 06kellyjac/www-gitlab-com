---
layout: markdown_page
title: "Triage Operations"
---

## On this page
{:.no_toc}

- TOC
{:toc}

Any GitLab team-member can triage issues. Keeping the number of un-triaged issues low is essential for maintainability, and is our collective responsibility.

We have implemented automation and tooling to handle this at scale and distribute the load to each team or group.

## Accountability

The Quality Engineering Department ensures that every Product and Engineering group is held accountable to deliver on the SLA set forth.

Our defect SLA can be viewed at:

* [Priority labels](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/development/contributing/issue_workflow.md#priority-labels)
* [Severity labels](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/development/contributing/issue_workflow.md#severity-labels)

The Quality Engineering department employs a number of tools and automation in addition to manual intervention to help us achieve this goal.
The work in this area can been seen in our department roadmap under [Triage](/handbook/engineering/quality/roadmap/#triage) and [Measure](/handbook/engineering/quality/roadmap/#measure) tracks of work.

## Stage and group labels inference from category labels

Our triage bot will automatically infer stage and group labels based on the
category/feature and team labels already set on an issue.

The most important rules are:

* The bot does doesn't change a stage or group label if it's already set.
* A group label is chosen only if the highest group match from its category labels is > 50%.
* A group label is chosen only if it matches the already set stage label (if applicable).
* A stage label is set based on the chosen or already set group label.
* The bot leaves a message that explains its inference logic.

The following logic was initially implemented in
[this merge request](https://gitlab.com/gitlab-org/quality/triage-ops/merge_requests/155#workflow):

```mermaid
graph TB;
  A{Stage label is present?} -- Yes --> B;
  B{Group label is present?} -- Yes --> X1[Nothing to do.];
  B -- No --> E;
  E{Group is detected based on category labels<br>with a match rate > 50% among all category labels?} -- Yes --> H;
  E -- No --> K;
  H{Does detected group label matches stage label?} -- Yes --> X2[Set detected group label.];
  H -- No --> K;
  K{Several potential groups in current<br>stage detected from category labels?} -- Yes --> X3[Manual triage required.];
  K -- No --> X4[Set default group label based on stage label.];
  A -- No --> C;
  C{Group label is present?} -- Yes --> X5[Set stage label based on group label, we're done!];
  C -- No --> G;
  G{Group is detected based on feature/category labels?} -- Yes --> X6[Set group and stage labels.];
  G -- No --> X7[Manual triage required.];
```

Check out the
[list of actual use-cases](https://gitlab.com/gitlab-org/quality/triage-ops/merge_requests/155#test-cases)
to better understand what this flow means in practice.

## Triage Packages

A [triage package](https://gitlab.com/gitlab-org/quality/team-tasks/issues/35) is an issue containing a checklist of issues requiring attention.
Each task corresponds to an issue that needs labels, prioritization and/or scheduling.

### Newly created unlabelled issues requiring initial triage

This package contains the 30 most recent unlabelled issues requiring initial triage.

* Triage owner: Quality Department Engineers.
* Triage action:
  1. Add a [type label](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/development/contributing/issue_workflow.md#type-labels).
    * If identified as a bug, add a [severity label](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/development/contributing/issue_workflow.md#severity-labels).
  1. Add a [team label](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/development/contributing/issue_workflow.md#team-labels)
  and [stage label](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/development/contributing/issue_workflow.md#stage-labels).
  1. If the issue is a request for help:
    * Use this template:
          ```
          This issue tracker is for Bug Reports and Feature Proposals relating to the GitLab Community Edition product. Unfortunately, the issue that you have posted is out of the scope of the Issue tracker. Fortunately, there are plenty of other resources that you can use to find help and support from the Community. Please read the [Getting Help](https://about.gitlab.com/getting-help) page of the GitLab website to find out more.

          /label ~"support request"
          /close
          ```
          
      * (Optional) Alternatively, instead of closing the issue when using the template above, you could take on the role of customer support and ask the reporter for more information so we can properly assist them. If you do this, add the ~"awaiting feedback" label.
  1. (Optional) Add relevant [subject labels](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/development/contributing/issue_workflow.md#subject-labels).
  1. (Optional) Mention relevant PM/EMs from the relevant stage group from [product devstages categories](https://about.gitlab.com/handbook/product/categories/#devops-stage).
* Enlist help as needed by mentioning folks in the [#triage](https://gitlab.slack.com/messages/C39HX5TRV) slack channel.
* Example: [https://gitlab.com/gitlab-org/gitlab-ce/issues/57834](https://gitlab.com/gitlab-org/gitlab-ce/issues/57834)

### Stage level issues

This package contains the relevant bugs and feature requests that belong to a
DevOps stage e.g. Manage, Create, Plan, Verify, etc.

The package itself is divided into 3 parts.
The first part contains feature proposals.
The second part contains frontend bugs.
The last part contains general bugs.

* Example: [https://gitlab.com/gitlab-org/quality/triage-ops/issues/118](https://gitlab.com/gitlab-org/quality/triage-ops/issues/118)

There is also an optional stage policy for [missing categories](https://gitlab.com/gitlab-org/quality/triage-ops/blob/master/policies/missing-categories.yml).
If your team has enabled this, you will receive a list of up to 100 items that have the stage label but have zero appropriate category labels for that stage.

#### Feature proposals

This section contains issues with the `~feature` label without a milestone.

* Triage owner: Product Manager(s) for that group.
* Triage actions:
  1. If the issue is a duplicate or irrelevant, close the issue out.
  1. Assign a milestone either to a versioned milestone, `Backlog` or `Awaiting further demand` milestone.

#### Frontend bugs

This section contains issues with the `~bug` and `~frontend` labels without priority and severity.

* Triage owner: Frontend Engineering Manager(s) for that group.
* Triage actions:
  1. Close the issue if it is no longer relevant or a duplicate.
  1. Assign a [Priority Label](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/development/contributing/issue_workflow.md#priority-labels).
  1. Assign a [Severity Label](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/development/contributing/issue_workflow.md#severity-labels).
  1. Assign either a versioned milestone or to the `Backlog`.

#### Bugs (likely backend)

This section contains issues with the `~bug` label without priority and severity.

* Triage owner: Backend Engineering Manager(s) for that group.
* Triage actions:
  1. Close the issue if it is no longer relevant or a duplicate.
  1. Assign a [Priority Label](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/development/contributing/issue_workflow.md#priority-labels).
  1. Assign a [Severity Label](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/development/contributing/issue_workflow.md#severity-labels).
  1. Assign either a versioned milestone or to the `Backlog`.

### Community merge requests requiring attention

This package contains open merge requests which has been submitted by the wider
community. These merge requests would have the ~"Community contribution" label.

The package itself is divided into 2 parts.
The first part contains the 20 newest merge requests from the wider community.
The second part contains 20 merge requests that weren't updated for 2 months or more.

  * Triage owner: [@gitlab-org/coaches](https://gitlab.com/gitlab-org/coaches).
  * Triage action:
    1. Determine if the merge request should be followed through or closed.
    1. Determine if the merge request is ready or further changes are required.
    1. Assign a reviewer as needed.
  * Example: [https://gitlab.com/gitlab-org/gitlab-ce/issues/58131](https://gitlab.com/gitlab-org/gitlab-ce/issues/58131)

## Resources

* [Issue Triage Policies](/handbook/engineering/issue-triage/).
* Chat channels; we use our chat internally as a realtime communication tool:
  * [#triage](https://gitlab.slack.com/messages/triage): general triage team channel.
  * [#gitlab-issue-feed](https://gitlab.slack.com/messages/gitlab-issue-feed) - Feed of all GitLab-CE issues
  * [#support-tracker-feed](https://gitlab.slack.com/messages/support-tracker-feed) - Feed of the GitLab.com Support Tracker
  * [#mr-coaching](https://gitlab.slack.com/messages/mr-coaching): for general conversation about Merge Request coaching.
  * [#opensource](https://gitlab.slack.com/messages/opensource): for general conversation about Open Source.
