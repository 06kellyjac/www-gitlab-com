---
layout: markdown_page
title: "NO.1.01 - Network Policy Enforcement Points Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# NO.1.01 - Network Policy Enforcement Points

## Control Statement

Network traffic to and from untrusted networks passes through a policy enforcement point; firewall rules are established in accordance to identified security requirements and business justifications.

## Context

Effective network traffic policies help minimize the risk of network-based attacks, including denial of service attacks and malicious data exfiltration. By requiring ingress and egress rules be mapped to security requirements and business justifications, we can limit the number of unnecessarily open ports to protect customer, GitLab team-member teammember, and partner data.

## Scope

This control applies to any GitLab system or infrastructure.

## Ownership

TBD

## Implementation Guidance

For detailed implementation guidance relevant to GitLab team-members, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/NO.1.01_network_policy_enforcement_points.md).

## Reference Links

For all reference links relevant to this control, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/NO.1.01_network_policy_enforcement_points.md).

## Examples of evidence an auditor might request to satisfy this control

For examples of evidence an auditor might request, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/NO.1.01_network_policy_enforcement_points.md).

## Framework Mapping

* ISO
  * A.13.1.1
* SOC2 CC
  * CC6.6
* PCI
  * 1.1.4
  * 1.2
  * 1.2.1
  * 1.2.3
  * 1.3
  * 1.3.1
  * 1.3.2
  * 1.3.3
  * 1.3.4
