---
layout: markdown_page
title: "Compliance for Your Average Human"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Compliance Topics Demystified 

## 1. Principle of Compliance

I had lunch with Mel Farber the other day.  She is such a healthy person that I thought it would be great to emulate such a pristine lifestyle. 

The waiter took our orders.  

Mel ordered a nice spinach salad.  Protein?  Yes, grilled salmon; complemented with cucumber, radishes, scallions, sprinkled with croutons with a cruet of oil and vinaigrette.  A French baguette on the side.  Mel was ready to embark on her healthy meal.

Me?  I ordered the same thing.  Except…I substituted the grilled salmon with fried chicken.  I replaced the croutons with French Fries and the baguette with biscuits. Oh, yeah, one more substitution - replace the oil and vinaigrette with gravy. Hold the cucumbers, radishes, and scallions.  I can do a spinach salad if modified correctly.

Mel seemed to like her salad.  Mine was okay...except for the spinach.

Sometimes we can find so many loopholes in the rules that we actually alter the underlying principle of the matter…and principles matter.  Everything we do matters. At GitLab, we want you to comply with the policies and procedures because it is the right thing to do; but most importantly, we want you to comply because we value integrity.

Paul Harvey once told an amazing story about a football ploy utilized by Carlisle University in the early 1900’s.  Carlisle was getting ready to play Syracuse.  Carlisle’s Coach Warner was determined to defeat Syracuse so he poured over the rule book to find any loophole he could exploit.  He found one.  Coach Warner had realistic footballs sewn onto the front of every player’s jersey on his team.  The football appliqués were puffed out so they actually appeared 3-dimensional.  The result is that when the game was in play, Syracuse couldn’t tell which Carlisle player actually had the ball.  The deceptive jerseys made it difficult to determine who to tackle.  The result – Carlisle won.

The next game was to be a heated one as it was between Carlisle and Harvard.  The two colleges were rivals and the stakes were high.  Despite the strong rivalry, the coaches respected one another. Harvard’s coach met with Coach Warner and inquired if he planned to have Carlisle don the deceptive uniforms in the upcoming game.  Coach Warner indicated that there were no rules that prevented him from doing so; so, yes, yes his team would be wearing the jerseys.  

Game day came.  Harvard was the home team.  Carlisle was the guest.  Carlisle took the field first in their appliquéd football garb.  Harvard then took the field in their **crimson** colored uniforms. As the home team, Harvard had the duty to bring the game balls.  The refs took the field and grabbed the bag of balls provided by the home team.  They reached in and discovered a bag full of **crimson** colored footballs.  

Harvard won. 

The point of the story is that people who use the rules as the baseline or absolute minimum for integrity are apt to look for loopholes.  Those who follow principles, do not.   Doing the right thing because it is the right thing ultimately wins the long game. 

## 2. GDPR

**GDPR is here…**
The General Data Protection Regulations came into effect in the European Union May, 2018.  Anyone who handles personal information of Europeans must comply.  In the European Union, privacy of personal information is a right – like human dignity, freedom, equality and the opposite of left.  It is a personal right belonging to the owner of the information and cannot be commoditized, utilized, processed without that person’s consent.  The European Union (EU) and European Economic Area (EEA) diligently try to preserve the rights of their citizens but such efforts were without coordinated and express direction – until now.  The GDPR reflects a unified approach to handling personal, sensitive information in the EU/EEA. 

**What is covered by the GDPR?**
The regulation is implemented in all local privacy laws across the entire EU and EEA region. It applies to all companies selling to and storing personal information about citizens in Europe, including companies on other continents (so even offices in Antarctica must comply if they intend to access personal information of Europeans.)  GDPR provides citizens of the EU and EEA with greater control over their personal data and assurances that their information is securely protected. According to the GDPR directive, personal data is any information related to a person such as a name, a photo, an email address, bank details, updates on social networking websites, location details, medical information, financial information or a computer IP address. Ultimately, data that can directly or indirectly identify a person is covered by GDPR.

**What do these regulations involve?** 
There are eight fundamental principles under the GDPR.  Let’s bring it down to a personal application to help navigate. Imagine you left your electronic diary on your Ex’s server.  You remember that diary; the one with all those deep, dark secrets like the time you went into a dressing room and then yelled real loudly, “There’s no toilet paper in here!”?  Those crazy times from your past can haunt you – unless you are European.  Well, they can still haunt you - but not electronically.  You want your diary back, you need it back and you need to know how much damage was done. Congratulations! In our hypothetical, you are European!  (You may be European outside of the hypothetical, as well, but stay with me here.) 

**Applying the GDPR to your diary, what are your rights?**

**The right to access** – Individuals have the right to request access to their personal data and to ask how their data is used by the company after it has been gathered. The company must provide a copy of the personal data, free of charge and in electronic
format if requested. 

* So, your diary is on your Ex's server.  You have the right to see exactly what dates and entries are stored.  And, of equal importance, you want to know, specifically, with whom your Ex shared that information and why. (Details!  You want details!) You would not want to hear a generic answer like “The contents of the diary were shared with the Finance department, the customers at the local pub and numerous bathroom stalls… for everyone’s reading enjoyment.”  You want specificity about what was shared and with whom. And there had better have been a good reason for any sharing that occurred. 

**The right to be forgotten** – Individuals can withdraw their consent from a company to use their personal data; they have the right to have their data erased.

*  You can demand your diary be shredded, burned, flambéed, sautéed with a nice Hollandaise sauce or simply destroyed (for the more likely and less dramatic).  In short, all evidence of the contents of your book must be erased.  There cannot be excerpts of the diary floating about on social media, bathroom walls or billboards.  The existence, use and memory of the information must disappear faster than cash in my wallet.  

**The right to data portability** – Individuals have a right to transfer their data from one service provider to another. And it must happen in a commonly used and machine readable format.

*  Maybe you separated on good terms and you think your Ex’s friend is pretty cute.  You can ask to have your diary delivered to that humorous, single friend in an unaltered, readable format.  Generally, this is a more common request with medical records, academic transcripts and the like but those aren't the subject of this hypothetical.

**The right to be informed** – this covers any gathering of data by companies.  Individuals must be informed before data is gathered. Individuals must opt in for their data to be gathered, and consent must be freely given rather than implied. 

*  If your Ex decides to go find evidence of your past based on your contents, you have to give permission for each planned encounter.  You can allow your Ex to visit your third grade teacher, obtain your 9th grade report card and visit your first childhood address.  Nothing more.  If your Ex decides to get details about your juggling statistics, your Ex crossed the line.  Just because your Ex has your diary- doesn’t mean you intend for the contents to be read, evaluated, processed, extrapolated or shared.  Consent must be specific, clear and express. There can’t be the broad, sweeping comment from your Ex that “I will only share your information on days that start with ‘T’… Today and Tomorrow; and only with people with a pulse.”  There must be clarity in how and with whom the information is shared.

**The right to have information corrected** – Individuals can have their data updated if it is out of date, incomplete or incorrect. 

*  You told your Ex that the diary excerpt from May 16th can be posted on Facebook.  Your Ex posts that you said “I have enough money to live happily the rest of my life...”  You can demand it be corrected to include the last part of the sentence which stated “...if I die on
Thursday.”      

**The right to restrict processing** – Individuals can request that their data is not used for processing. Their record can remain in place, but not be used. 

*  Your diary can stay on your Ex's server but it better not be shared, analyzed or used for anything.  If your Ex decides to compile a little black book blog and wants to invoke a ranking system with stars based on the entries in your diary – you can refuse.   

**The right to object** – this includes the right of individuals to stop the processing of their data for direct marketing. There are no exemptions to this rule, and any processing must stop as soon as the request is received. In addition, this right must be made clear to individuals at the very start of any communication.

*  If you find out your Ex is, in fact, continuing with the little black book blog – you can demand a complete stop.  Your Ex cannot object or try to find some loophole.  The diary is closed.  The conversation is closed.  The end.

**The right to be notified** – If there has been a data breach which compromises an individual’s personal data, the individual has a right to be informed within 72 hours of first having become aware of the breach. 

*  If your Ex left your diary open and unencrypted on a laptop at a cafe, your Ex better tell you quickly and with as much detail as possible of the events surrounding the loss of the sensitive information. Your Ex should expect reprisals for any careless behavior that alerted some third party to your private life.

Marc Antony said in Shakespeare's Julius Caesar, “The evil that men do lives after them.  The good is oft interred with their bones.”  Poor Caesar should’ve waited for the GDPR.  He could have better controlled the information dispersed about him.  Good thing he didn’t have a diary.

## 3. Secrets

I’m only sharing this with you because I trust you.  I know that you know how to keep secrets. Do you realize how many secrets you are entrusted with each day?  There are three primary categories of company secrets that are super special: Trade Secrets, Confidential Information and Personally Identifiable Information (Sensitive Information if you parlez OUS).  Collectively, these things are classified as RED in our Data Classification Policy.  The “Red” designation is for the most sensitive, restricted business information that requires heightened internal controls by limiting access to only those authorized individuals with a need-to-know.  Not everyone gets to know this inside scoop.  

**Trade Secrets** are those extra bits of information that set our company apart – the chocolate chips to our cookies.  It’s that stuff that is so cutting edge that we don’t even want to get a patent on it for fear of letting others know the technology.  If you patent something, the secret ingredients are laid out for the world to see like some crazy technological recipe.  Others are precluded from copying your recipe but once they see the basic ingredients, it doesn’t take long to realize that butter can be replaced with oil;  basic flour and yeast can be replaced with self-rising flour and sugar can be replaced with gummy bears (clearly, you do not want to eat my cooking.)   Competitors quickly mobilize for comparable product offerings.  But! where there is a trade secret, competitors are left scratching their heads. Have you ever wondered what the recipe is for Coca-Cola?  Do you realize that you don’t know the recipe because the employees are very good about protecting its secrets.  There is no patent.  If there was, we would all be drinking knock off Cokes that taste like the real thing.  Once you release a trade secret – there is no protection.  It is all out there for the world to exploit; this is why we have employees sign non-disclosures and occasionally pink swear to protect the information. 

Another set of secrets include **Confidential Information**.  This is more than the general confidential stuff.  This includes the provocative business data that helps us run strategically and includes business development plans/strategies, non-public financial information, third party data and government protected information.  (Trade Secrets technically fall under this category but given its unique stature, it gets celebrity status.)  Failing to protect heightened confidential information could cause all sorts of bad feelings with GDPR, EUCI, SOX, EUEC, DOJ, DOC, SEC, OAIC and others.  In other words, if you fail to execute proper controls and restraint, you will be attacked by nothing short of alphabet soup with a badge. 

Other secrets include **Personally Identifiable Information (PII)** [parfois connu comme Sensitive Information].  This is any information that links a particular piece of sensitive information to a specific person.  For instance, if I tell you someone makes a million dollars a year.  You would probably respond with “Thank you, Captain Obvious.” That statement isn't really PII.  It's rather generic and could apply to anyone (unfortunately, not me.)  If I tell you that Jamie makes a million dollars a year - that information is interesting and even intriguing; but this, in and of itself, in not PII. There are a million Jamies in the world, it could be anyone.  Now, if I told you that Jamie Hurewitz makes a million dollars year, I have now divulged PII.  You are now able to link a specific piece of sensitive information to a particular person.  This information becomes something you can exploit.  Now that you know this information - you are going to want to befriend Jamie, have her take you out to dinner, have her cosign a loan, apply for her position…  She has now been stripped of the privacy that she probably wanted to maintain. If my salary statement were true, I would be in serious trouble. 

There are various types of PII.  There is Personal Health Information (PHI) which includes sensitive information about a person’s health or medical condition; Personal Credit Information (PCI) which includes credit card information; Personally Identifiable Financial Information (PIFI) which includes banking information, account numbers, and account information; PI which is 3.14159265359; and PIE which is a more conservative dessert approach. [The secret is already out on these last two.]  How do you protect PII?  Consider the guidelines in the Data Classification Policy and apply it to all of the PII listed above (except PI whose protection is irrational and PIE whose protection won’t last five minutes unattended in my house).  

Unauthorized disclosure of Red, restricted information, could cause serious adverse impact to GitLab, its clients, business partners, and suppliers.  Your mission, should you decide to accept it…and you should accept it (not really optional), is to ensure proper protection for all data classified as Red.  

As always, feel free to reach out with questions or concerns. This message will self-destruct in five minutes.

## 4. Personal Credit Information (PCI)

My credit card was stolen last week.  My husband isn’t reporting it though because, apparently, the thief spends less than I do.    It’s not that I’m frivolous with my spending, it’s just that there is Nigerian prince that I’ve been helping out.  I don't like to talk about it.  

Credit card information falls into the bucket of Personal Credit Information (PCI).  PCI requires heightened levels of protection - much like Personal Health Information (PHI), Personal Identifiable Information (PII), Personal Financial Information (PFI) and other Sensitive Information.  PCI is restricted information with a data classification of Red. PCI isn’t only governed by law; it is also heavily regulated by the credit card industry. Failure to properly handle credit card or bank information could result in the company not being able to process credit card orders.  Nobody wants to resort to checks – that is sooo 1980’s.  So, please, keep in mind - and apply - the Data Classifcation requirements for a Red level when dealing with PCI, PHI, PFI, PII, trade secrets and those other juicy bits of information we are bound to protect. 

The fundamental requirements for PCI are:
1. Must have a maintained firewall;
2. Custom passwords and unique security measures;
3. Secure/encrypt cardholder data at rest;
4. Encrypt any cardholder data transmitted;
5. Current, active and updated anti-virus must be installed;
6. Secured systems and applications must be sustained;
7. Access is on a need-to-know basis only;
8. Unique data identifiers are required for anyone who has access to cardholder data;
9. Physical access to cardholder data must be restricted;
10. Access to cardholder data needs to be logged and reported;
11. Ensure that security system and process tests are run frequently; and
12. There should be policies around all of the above.

When dealing with PCI, at no time should an entire credit card number be visible in its full form.  Generally, only the last four numbers of a card should be maintained.  Please make sure that there is no credit information ever jotted down on sticky notes, files, napkins, etc.  If you are aware of any credit card numbers that are not secure, please alert Compliance. 

As a young child, my mother told me I can be anyone I want to be...turns out this is called identity theft.  Don’t go down that road.  Let’s keep things on the up and up.